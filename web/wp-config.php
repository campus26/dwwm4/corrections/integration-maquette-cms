<?php
if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') $_SERVER['HTTPS'] = 'on';


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', getenv("MYSQL_DB"));

/** Database username */
define('DB_USER', getenv("MYSQL_USER"));

/** Database password */
define('DB_PASSWORD', getenv("MYSQL_PASSWORD"));

/** Database hostname */
define('DB_HOST', getenv("MYSQL_HOST"));

/** Database charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The database collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D 9S-eu-s:|Z9DsfQV:BbJw R.f.>yv0cgM>e#>F;3Iz>:lIVwCU2oX,q`< ;Uj^');
define('SECURE_AUTH_KEY',  'V;^4j-xUOcV/RIiF9vI9n9()2Og7d@=9b7=0N0]$5mq@3eQ)S0G).&RJV3W,e.Dt');
define('LOGGED_IN_KEY',    '&qrpB(#tfN]|YJkZ^3IFyj@r=`.V PGGv86b>&czYxwp& gyWy5]P#[eFOXc8aBg');
define('NONCE_KEY',        '-=9`TCePO#3,ulhgw]vR+6?)ZYh|I7JZQ)$P-$qFBAYL1XYze/O@KEsmq2]j:ki~');
define('AUTH_SALT',        'J4l_zSt9)/i#02dV0C97G:{AWDr z`E%bEGAamPuDAf^:py707Uog+44`O]Uw{;#');
define('SECURE_AUTH_SALT', 'Dn;1iM)^s, d}Us8W|OarH#vyeusj8~<W#9etE^#Gu0#GEQ7i%k={$@1|aja$Y)@');
define('LOGGED_IN_SALT',   '<+[JTqiw<q)b+.taSo#CxuUodJQ;|lDO`+TpF(y(t:a,RZz`,ax=zwb0Gu7T7gv/');
define('NONCE_SALT',       'Pppk$r>.c%)WBk>}Z{;lRNSa7c%LBi^sAhjPf+>}1*q1(XTJuC`Bv]Lj/;LZ!}sS');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'cp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define('WP_DEBUG', true);

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
