       <!-- About area start -->
       <section class="about-wrap pt-120 pb-120">
           <div class="container">
               <ul class="bg-shapes-top">
                   <li></li>
                   <li></li>
                   <li></li>
                   <li></li>
               </ul>
               <ul class="bg-shapes-bottom">
                   <li></li>
                   <li></li>
                   <li></li>
                   <li></li>
               </ul>
               <div class="section-title-wrap text-center">
                   <h2 class="section-title">Qui suis je ?</h2>
                   <p>Vous ne me connaissez pas ? Je suis Anto. travailleur du Digital, petite main dans l'intégration et le développement,
                       je suis actuellement disponible pour vous aidez dans vos projets en cours et à venir.
                   </p>
               </div>

               <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-12">
                       <div class="counter-wrap">
                           <div class="counter-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/ongoing-project.png" alt="Ongoing projects">
                           </div>
                           <div class="counter">
                               <h4>20+</h4>
                           </div>
                           <h5>Projets des 10 derniers mois</h5>
                       </div>
                   </div>
                   <div class="col-lg-4 col-md-4 col-sm-12">
                       <div class="counter-wrap">
                           <div class="counter-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/completed-task.png" alt="Completed projects">
                           </div>
                           <div class="counter">
                               <h4>200+</h4>
                           </div>
                           <h5>Nb de Cafés pour finaliser les projets</h5>
                       </div>
                   </div>
                   <div class="col-lg-4 col-md-4 col-sm-12">
                       <div class="counter-wrap">
                           <div class="counter-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/happy-clients.png" alt="Happy clients"></div>
                           <div class="counter">
                               <h4>8+</h4>
                           </div>
                           <h5>Nb de compétences acquises durant mes formations</h5>
                       </div>
                   </div>

               </div>
               <div class="text-center button-wrapper mr-top-30">
                   <a class="button button-yellow" href="<?php echo get_template_directory_uri(); ?>/about.html">Read more</a>
               </div>
           </div>
       </section>
       <!-- About area end -->