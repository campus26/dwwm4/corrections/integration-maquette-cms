<!-- Preloader Start -->

<div id="preloader">

    <div class="preloader-container">
        <div class="coast">
            <div class="wave-rel-wrap">
                <div class="wave"></div>
            </div>
        </div>
        <div class="coast delay">
            <div class="wave-rel-wrap">
                <div class="wave delay"></div>
            </div>
        </div>
        <div class="preloader-text preloader-text-1">L</div>
        <div class="preloader-text preloader-text-2">O</div>
        <div class="preloader-text preloader-text-3">A</div>
        <div class="preloader-text preloader-text-4">D</div>
        <div class="preloader-text preloader-text-5">I</div>
        <div class="preloader-text preloader-text-6">N</div>
        <div class="preloader-text preloader-text-7">G</div>
    </div>


</div>
<!-- Preloader end -->