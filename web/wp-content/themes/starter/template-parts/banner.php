<?php
$args = array(
    'post_type'      => 'project',         // Type de publication : Article
    'posts_per_page' => 5,              // Nombre d'articles à afficher
    'orderby'        => 'date',         // Trier par date
    'order'          => 'DESC'          // Ordre décroissant (les plus récents en premier)
);

$projects = new WP_Query($args);

?>

<!-- Banner area start -->

<section class="banner-wrap pb-130">
    <!-- Slider main container -->
    <div class="swiper">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php
            if ($projects->have_posts()) :
                while ($projects->have_posts()) : $projects->the_post();
                    // Afficher le contenu de chaque article
            ?>

                    <div class="swiper-slide slide" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>, background-size:cover;">
                        <div class="slide-content">

                            <p><a class="slide-title-1" href="#"><?php the_content(); ?></a></p>

                            <h1 class="slide-title-2">
                                <?php the_title(); ?>
                            </h1>

                            <div class="button-wrapper">
                                <a class="button" href="<?php echo get_the_permalink(); ?>">Infos du projet</a>
                            </div>

                        </div>
                    </div>
                    <p><?php the_excerpt(); ?></p>
            <?php
                endwhile;
                wp_reset_postdata(); // Réinitialiser les données postérieures pour d'autres requêtes
            else :
                echo 'Aucun article trouvé.';
            endif;
            ?>

        </div>
        <!-- pagination -->
        <div class="swiper-pagination"></div>

        <!-- navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

    </div>

</section>
<!-- Banner area end -->