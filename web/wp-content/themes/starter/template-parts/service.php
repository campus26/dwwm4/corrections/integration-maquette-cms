        <!-- Service area start -->
        <section class="service-wrap pt-120 pb-120" style="background-color: #f8f6f3;">
            <div class="container">
                <div class="section-title-wrap text-center">
                    <h2 class="section-title"><span class="section-title-text">What we do?</span></h2>
                    <p>From website design to search engine optimization, social media marketing, and more, we offer a wide
                        range of services to help you achieve your digital goals.</p>

                </div>
                <div class="row text-center">

                    <div class="col-lg-3 col-md-6">
                        <div class="service-box">
                            <div class="box spin circle">
                                <lottie-player tabindex="0" src="<?php echo get_template_directory_uri(); ?>/assets/json/website.json" background="transparent" speed="1" style="width: 160px; height: 160px;" hover role="img" aria-label="Animation of Website and Mobile Apps service"></lottie-player>
                            </div>
                            <h5><a href="<?php echo get_template_directory_uri(); ?>/service-single.html">Website and Mobile Apps</a></h5>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-6">
                        <div class="service-box">
                            <div class="box spin circle">
                                <lottie-player tabindex="0" src="<?php echo get_template_directory_uri(); ?>/assets/json/social_media.json" background="transparent" speed="1" style="width: 160px; height: 160px;" hover loop role="img" aria-label="Animation of Social Media Marketing service"></lottie-player>

                            </div>
                            <h5><a href="<?php echo get_template_directory_uri(); ?>/service-single.html">Social Media Marketing</a></h5>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-6">
                        <div class="service-box">
                            <div class="box spin circle">
                                <lottie-player tabindex="0" src="<?php echo get_template_directory_uri(); ?>/assets/json/cms.json" background="transparent" speed="1" style="width: 160px; height: 160px;" hover loop role="img" aria-label="Animation of CMS Implementation service"></lottie-player>

                            </div>
                            <h5><a href="<?php echo get_template_directory_uri(); ?>/service-single.html">CMS Implementation</a></h5>
                        </div>
                    </div>



                    <div class="col-lg-3 col-md-6">
                        <div class="service-box">
                            <div class="box spin circle">
                                <lottie-player tabindex="0" src="<?php echo get_template_directory_uri(); ?>/assets/json/e-commerce.json" background="transparent" speed="1" style="width: 160px; height: 160px;" hover loop role="img" aria-label="Animation of e-commerce solutions service"></lottie-player>

                            </div>
                            <h5><a href="<?php echo get_template_directory_uri(); ?>/service-single.html">E-commerce solutions</a></h5>
                        </div>
                    </div>



                </div>

            </div>
        </section>
        <!-- Service area end -->