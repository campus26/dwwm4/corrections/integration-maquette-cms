<?php get_header(); ?>


<!-- Main page wrapper start -->
<div class="page-wrapper">



    <?php get_template_part('nav'); ?>

    <!-- Page content area start -->
    <main>

        <!-- Breadcrumb start -->
        <section class="breadcrumb-wrap" aria-label="Breadcrumb" style="background-image:linear-gradient(black, black), url(assets/images/bg/news-page.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="breadcrum-title-wrap">
                            <h1 class="breadcrumb-title"><?php echo get_the_title(); ?></h1>
                        </div>
                        <nav class="breadcrumb-path" aria-label="Breadcrumb">
                            <ol>
                                <li><a href="index.html">Home</a></li>
                                <li><a href="news-single.html">News</a></li>
                                <li>News Single</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb end -->

        <!-- News area start -->
        <section class="content-page pt-120 pb-120" aria-label="Detailed News">
            <div class="container">
                <ul class="bg-shapes-top">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <ul class="bg-shapes-bottom">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <div class="row">

                    <div class="col-12 content-page-wrap">
                        <article>
                            <div class="content-page-image">
                                <img src="assets/images/news/news-single.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="content-page-text">
                                <?php echo get_the_content(); ?>
                            </div>
                            <div class="content-page-footer">
                                <div class="content-page-tags">
                                    <label class="label">Tags:</label>
                                    <a href="#" rel="tag">Creative</a>
                                    <a href="#" rel="tag">Design</a>
                                    <a href="#" rel="tag">Web</a>
                                </div>
                                <div class="content-page-social">
                                    <label>Share:</label>
                                    <a aria-haspopup="true" title="Share on Facebook" target="_blank" href="#">
                                        <i class="icofont-facebook"></i>
                                        <span class="sr-only">Share on Facebook</span>
                                    </a>
                                    <a aria-haspopup="true" title="Share on Twitter" target="_blank" href="#">
                                        <i class="icofont-twitter"></i>
                                        <span class="sr-only">Share on Twitter</span>
                                    </a>
                                    <a aria-haspopup="true" title="Share on LinkedIn" target="_blank" href="#">
                                        <i class="icofont-linkedin"></i>
                                        <span class="sr-only">Share on LinkedIn</span>
                                    </a>
                                </div>
                            </div>

                        </article>
                    </div>
                </div>


            </div>
        </section>
        <!-- News area end -->
    </main>
    <!-- Page content area end -->

    <!-- Gooey filter start -->
    <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop" />
            </filter>
        </defs>
    </svg>
    <!-- Gooey filter end -->

</div>
<!-- Main page wrapper end -->
<?php get_footer(); ?>