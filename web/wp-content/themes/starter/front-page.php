<?php get_header(); ?>


<?php get_template_part('template-parts/preloader'); ?>

<!-- Main page wrapper start -->
<div class="page-wrapper">

    <?php get_template_part('nav'); ?>

    <!-- Page content area start -->
    <main>


        <?php get_template_part('template-parts/banner'); ?>
        <?php get_template_part('template-parts/about'); ?>
        <?php get_template_part('template-parts/service'); ?>

        <!-- Process area start -->
        <section class="process-wrap pt-120 pb-120">
            <div class="container">
                <ul class="bg-shapes-top">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <ul class="bg-shapes-bottom">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <div class="section-title-wrap text-center">
                    <h2 class="section-title"><span class="section-title-text"><?php the_field('main_title'); ?></span></h2>
                    <p>We take a customer-centric approach to every project, working closely with you to understand your unique
                        needs and goals. Our goal is to help you create a memorable and positive digital experience for your
                        customers, while also driving conversions and growing your business.</p>

                </div>
                <div class="row">
                    <div class="col-xxl-12">
                        <div class="process-content">
                            <div class="container-fluid">
                                <ul class="process-list">
                                    <li class="process">
                                        <div class="process-circle spin circle">
                                            <span>1</span>
                                            <div class="zigzax-line"></div>
                                        </div>
                                        <div class="info">
                                            <h5 class="title">receive request</h5>
                                            <p>Sed sed turpis euismod, interdum quam eu, finibus felis. Nam mollis arcu.</p>
                                        </div>
                                    </li>
                                    <li class="process">
                                        <div class="process-circle  spin circle">
                                            <span>2</span>
                                            <div class="zigzax-line"></div>
                                        </div>
                                        <div class="info">
                                            <h5 class="title">Study the application</h5>
                                            <p>Sed sed turpis euismod, interdum quam eu, finibus felis. Nam mollis arcu.</p>
                                        </div>
                                    </li>
                                    <li class="process">
                                        <div class="process-circle  spin circle">
                                            <span>3</span>
                                            <div class="zigzax-line"></div>
                                        </div>
                                        <div class="info">
                                            <h5 class="title">Initial meeting</h5>
                                            <p>Sed sed turpis euismod, interdum quam eu, finibus felis. Nam mollis arcu.</p>
                                        </div>
                                    </li>
                                    <li class="process">
                                        <div class="process-circle  spin circle">
                                            <span>4</span>
                                            <div class="zigzax-line"></div>
                                        </div>
                                        <div class="info">
                                            <h5 class="title">Studies & analysis</h5>
                                            <p>Sed sed turpis euismod, interdum quam eu, finibus felis. Nam mollis arcu.</p>
                                        </div>
                                    </li>
                                    <li class="process">
                                        <div class="process-circle  spin circle">
                                            <span>5</span>
                                            <div class="zigzax-line"></div>
                                        </div>
                                        <div class="info">
                                            <h5 class="title">Present initial concept</h5>
                                            <p>Sed sed turpis euismod, interdum quam eu, finibus felis. Nam mollis arcu.</p>
                                        </div>
                                    </li>
                                    <li class="process">
                                        <div class="process-circle spin circle">
                                            <span>6</span>
                                            <div class="zigzax-line"></div>
                                        </div>
                                        <div class="info">
                                            <h5 class="title">Implement</h5>
                                            <p>Sed sed turpis euismod, interdum quam eu, finibus felis. Nam mollis arcu.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Process area end -->

        <!-- Project area start -->
        <section class="project-wrap  pt-120 pb-120" style="background-color: #f8f6f3;">
            <div class="container">
                <div class="section-title-wrap text-center">
                    <h2 class="section-title"><span class="section-title-text">Qui suis je ?</span></h2>
                    <p>We believe that every project is an opportunity to create something exceptional, and we're passionate
                        about delivering results that exceed our clients' expectations. Browse through our portfolio to see what
                        we can do for you and let's create something amazing together!</p>

                </div>

                <div class="row project-slide">
                    <div class="col-lg-8 project-image">
                        <div class="project-thumb project-thumb-carousel">
                            <div class="swiper-wrapper" role="list">
                                <div class="swiper-slide" role="listitem" aria-label="Mobile App">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/projects/project-mobile-app1.jpg" class="img-fluid" alt="Mobile App">
                                </div>
                                <div class="swiper-slide" role="listitem" aria-label="Branding">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/projects/project-branding-1.jpg" class="img-fluid" alt="Branding">
                                </div>
                                <div class="swiper-slide" role="listitem" aria-label="Web Project">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/projects/project-web-1.jpg" class="img-fluid" alt="Web Project">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex project-content-wrap">
                        <div class="project-content project-content-carousel">
                            <div class="swiper-wrapper" role="list">
                                <div class="swiper-slide" role="listitem" aria-label="Mobile App">
                                    <div class="project-content-single">
                                        <h3 class="project-content-title">Mobile App</h3>
                                        <p class="project-content-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                            nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                                        <span class="project-content-line"></span>
                                        <a href="<?php echo get_template_directory_uri(); ?>/projects.html" role="button" aria-label="More projects" class="btn btn-project">More
                                            projects</a>

                                    </div>
                                </div>
                                <div class="swiper-slide" role="listitem" aria-label="Branding">
                                    <div class="project-content-single">
                                        <h3 class="project-content-title">Branding</h3>
                                        <p class="project-content-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                            nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                                        <span class="project-content-line"></span>
                                        <a href="<?php echo get_template_directory_uri(); ?>/projects.html" role="button" aria-label="More projects" class="btn btn-project">More
                                            projects</a>

                                    </div>
                                </div>
                                <div class="swiper-slide" role="listitem" aria-label="Web Project">
                                    <div class="project-content-single">
                                        <h3 class="project-content-title">Web Project</h3>
                                        <p class="project-content-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                            nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                                        <span class="project-content-line"></span>
                                        <a href="<?php echo get_template_directory_uri(); ?>/projects.html" role="button" aria-label="More projects" class="btn btn-project">More
                                            projects</a>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="swiper-pagination swiper-project-pagination"></div>

                </div>
            </div>
        </section>

        <!-- Project area end -->

        <!-- Contact area start -->
        <section class="contact-wrap pt-120 pb-120">
            <div class="container">
                <div class="section-title-wrap text-center">
                    <h2 class="section-title"><span class="section-title-text">How To Contact Us?</span></h2>
                    <p>Whether you're looking to revamp your website, improve your search engine rankings, or launch a new
                        digital marketing campaign, we're here to help. Ready to take your digital presence to the next
                        level?Reach out to us today and let's create something amazing together!</p>

                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="button-wrapper">
                            <a class="button button-yellow" href="<?php echo get_template_directory_uri(); ?>/contact.html" title="Contact Us">call us</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact area end -->

        <!-- News area start -->
        <section class="news-wrap  pt-120 pb-120">
            <div class="container">
                <ul class="bg-shapes-top">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <ul class="bg-shapes-bottom">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <div class="section-title-wrap text-center">
                    <h2 class="section-title"><span class="section-title-text">Mes derniers projets</span></h2>
                    <p>From new web design trends to the latest social media algorithms, we cover it all. So, whether you're a
                        business owner, marketer, or designer, our news section is the go-to resource for all things digital.
                        Check back regularly for new content and insights, and let's stay ahead of the game together!</p>

                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img1.jpg" alt="Lorem ipsum dolor">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="<?php echo get_template_directory_uri(); ?>/news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="<?php echo get_template_directory_uri(); ?>/news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="<?php echo get_template_directory_uri(); ?>/news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="<?php echo get_template_directory_uri(); ?>/news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img2.jpg" alt="">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="<?php echo get_template_directory_uri(); ?>/news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="<?php echo get_template_directory_uri(); ?>/news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="<?php echo get_template_directory_uri(); ?>/news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="<?php echo get_template_directory_uri(); ?>/news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex newsletter-block">
                        <div class="newsletter-block-wrap">
                            <div class="newsletter-title-wrap" role="heading" aria-label="Newsletter Title">
                                <div class="newsletter-title-icon"><span><i class="icofont-envelope"></i></span></div>
                                <h2 class="newsletter-title-text">Subscribe to
                                    Our newsletter</h2>
                            </div>
                            <div class="newsletter-subtitle-wrap" role="heading" aria-label="Newsletter Subtitle">
                                Subscribe to get news and updates straight to your inbox
                            </div>
                            <div class="newsletter-form-wrap" role="form" aria-label="Newsletter Form">
                                <form>
                                    <label for="email-address" class="sr-only">Enter your Email Address</label>
                                    <input type="email" id="email-address" name="email-address" placeholder="example@mail.com" required>
                                    <button type="submit" class="button-reset newsletter-submit" aria-label="Submit Newsletter Form">Submit</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- News area end -->

        <!-- Testimonial area start -->
        <section class="testimonial-wrap pt-120 pb-120" style="background-color: #f8f6f3;">
            <div class="container">
                <div class="section-title-wrap text-center">
                    <h2 class="section-title"><span class="section-title-text">Words from satisfied customers</span></h2>
                    <p>We take pride in delivering exceptional results and creating positive digital experiences for our
                        clients. Don't just take our word for it - read what our clients have to say about working with us. </p>

                </div>

                <div class="testimonial-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="testimonials-single">

                                <div class="testimonials-text">
                                    <span aria-hidden="true" class="testimonials-quote-top-wrap"><i class="icofont-quote-left"></i></span>
                                    <p>Maha Digital Agency is hands down the best digital agency we've worked
                                        with. Their team is incredibly talented, responsive, and dedicated to delivering results. They
                                        revamped our website and created a comprehensive digital marketing strategy that has led to a
                                        significant increase in traffic and conversions. We couldn't be happier with their services!</p>
                                    <span aria-hidden="true" class="testimonials-quote-wrap"><i class="icofont-quote-right"></i></span>
                                </div>
                                <h3 class="testimonials-title">- Emily, Marketing Director</h3>

                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="testimonials-single">
                                <div class="testimonials-text">
                                    <span aria-hidden="true" class="testimonials-quote-top-wrap"><i class="icofont-quote-left"></i></span>
                                    <p>Maha Digital Agency is hands down the best digital agency we've worked
                                        with. Their team is incredibly talented, responsive, and dedicated to delivering results. They
                                        revamped our website and created a comprehensive digital marketing strategy that has led to a
                                        significant increase in traffic and conversions. We couldn't be happier with their services!</p>
                                    <span aria-hidden="true" class="testimonials-quote-wrap"><i class="icofont-quote-right"></i></span>
                                </div>
                                <h3 class="testimonials-title">- David, CEO</h3>

                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="testimonials-single">
                                <div class="testimonials-text">
                                    <span aria-hidden="true" class="testimonials-quote-top-wrap"><i class="icofont-quote-left"></i></span>
                                    <p>Maha Digital Agency is hands down the best digital agency we've worked
                                        with. Their team is incredibly talented, responsive, and dedicated to delivering results. They
                                        revamped our website and created a comprehensive digital marketing strategy that has led to a
                                        significant increase in traffic and conversions. We couldn't be happier with their services!</p>
                                    <span aria-hidden="true" class="testimonials-quote-wrap"><i class="icofont-quote-right"></i></span>
                                </div>
                                <h3 class="testimonials-title">- Rachel, Marketing Manager</h3>

                            </div>
                        </div>
                    </div>
                    <div class="swiper-testimonial-pagination"></div>
                </div>

            </div>

        </section>

        <!-- Testimonial area end -->

        <!-- Clients area start -->
        <section class="client-wrap pt-0 pb-120" style="background-color: #f8f6f3;">
            <div class="container">
                <div class="section-title-wrap text-center">
                    <h2 class="section-title"><span class="section-title-text">Our Prestigious Clients</span></h2>
                </div>

                <div class="client-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/1.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/2.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/3.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/4.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/5.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/1.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/2.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/3.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/4.png" alt="client logo">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client-logo-wrap">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/clients/5.png" alt="client logo">
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </section>

        <!-- Clients area end -->

        <!-- Footer area start -->
        <footer class="site-footer footer-bg">

            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 text-center text-lg-start">
                            <div class="footer-widget">
                                <a href="<?php echo get_template_directory_uri(); ?>/index.html" class="footer-logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Logo Image"></a>
                            </div>
                        </div>
                        <div class="col-lg-6 text-center text-lg-start">
                            <ul class="footer-address">
                                <li><a href="<?php echo get_template_directory_uri(); ?>/tel:+123456789">+123 456 789</a></li>
                                <li><a href="<?php echo get_template_directory_uri(); ?>/mailto:hello@example.com">hello@example.com</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-4 text-center text-lg-end">
                            <div class="button-block">
                                <span class="btn-tagline">For free quote
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/arrow.png" alt=""></span>
                                <div class="button-wrapper">
                                    <a class="button button-yellow" href="<?php echo get_template_directory_uri(); ?>/contact.html">contact
                                        us</a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 text-center text-md-start">
                            <ul class="social">
                                <li>
                                    <a href="<?php echo get_template_directory_uri(); ?>/#" aria-haspopup="true" target="_blank" class="instagram" aria-label="Instagram">
                                        <i class="icofont-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo get_template_directory_uri(); ?>/#" aria-haspopup="true" target="_blank" class="facebook" aria-label="Facebook">
                                        <i class="icofont-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo get_template_directory_uri(); ?>/#" aria-haspopup="true" target="_blank" class="twitter" aria-label="Twitter">
                                        <i class="icofont-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo get_template_directory_uri(); ?>/#" aria-haspopup="true" target="_blank" class="linkedin" aria-label="LinkedIn">
                                        <i class="icofont-linkedin"></i>
                                    </a>
                                </li>
                            </ul>


                        </div>
                        <div class="col-md-6 text-center text-md-end">
                            <div class="copyright">&copy; 2023 Maha. All Rights Reserved.</div>
                        </div>
                    </div>
                </div>
            </div>

        </footer>
        <!-- Footer area end -->

    </main>
    <!-- Page content area end -->

    <!-- Gooey filter start -->
    <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop" />
            </filter>
        </defs>
    </svg>
    <!-- Gooey filter end -->

</div>
<!-- Main page wrapper end -->
<?php get_footer(); ?>