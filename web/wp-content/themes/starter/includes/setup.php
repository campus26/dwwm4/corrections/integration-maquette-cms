<?php

// Initialisation des fonctionnalités de mon thème
function oktopod_setup()
{
    add_theme_support('custom-logo');
    add_theme_support('post-thumbnails');

    register_nav_menus(
        array(
            'mainmenu' => _('Menu Principal'),
            'footermenu' => _('Menu Pied de Page')
        )
    );
}
add_action('init', 'oktopod_setup');
