<?php

// Initialisation des fonctionnalités de mon thème
function oktopod_posttype()
{
    $labels = array(
        'all_items'           => __('Tous les projets'),
        'view_item'           => __('Voir'),
        'add_new_item'        => __('Ajouter un nouveau projet'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer'),
        'update_item'         => __('Modifier'),
        'search_items'        => __('Rechercher'),
        'not_found'           => __('Non trouvée'),
        'not_found_in_trash'  => __('Non trouvée dans la corbeille'),
    );

    // FORMATION
    register_post_type(
        'project',
        array(
            'label' => __('Projets'),
            'description' => __('Tous les projets'),
            'labels' => $labels,
            'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'author'),
            /*
* Différentes options supplémentaires
*/
            'show_in_rest' => true,
            'hierarchical' => false,
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'projets'),
            'menu_icon' => 'dashicons-welcome-learn-more', // see at https://developer.wordpress.org/resource/dashicons/#email
        )
    );

    $args = array(
        'label' => __('Type de projet'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => false,
        'rewrite' => array('slug' => 'type-de-document'),
        'show_in_rest' => true,
    );
    register_taxonomy('category-projet', 'projet', $args);
}
add_action('init', 'oktopod_posttype');
