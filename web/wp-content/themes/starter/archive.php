<?php get_header(); ?>


<!-- Main page wrapper start -->
<div class="page-wrapper">



    <?php get_template_part('nav'); ?>

    <!-- Page content area start -->
    <main>
        <!-- Breadcrumb start -->
        <section class="breadcrumb-wrap" aria-label="Breadcrumb" style="background-image:linear-gradient(black, black), url(assets/images/bg/news-page.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="breadcrum-title-wrap">
                            <h1 class="breadcrumb-title">News</h1>
                        </div>
                        <nav class="breadcrumb-path" aria-label="Breadcrumb">
                            <ol>
                                <li><a href="index.html">Home</a></li>
                                <li>News</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb end -->

        <!-- News area start -->
        <section class="news-wrap  pt-120 pb-120">
            <div class="container">
                <ul class="bg-shapes-top">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <ul class="bg-shapes-bottom">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <div class="row">

                    <div class="col-lg-4 mb-5">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img1.jpg" alt="">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 mb-5">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img2.jpg" alt="">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-4 mb-5">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img1.jpg" alt="">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4  mb-5">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img1.jpg" alt="">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4  mb-5">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img2.jpg" alt="">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-4 mb-5">
                        <div class="news-box">
                            <div class="news-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/news/news-img1.jpg" alt="">
                                <div class="news-date"><i class="icofont-calendar"></i><time datetime="2023-04-29">April 29,
                                        2023</time></div>
                                <h5><a href="news-single.html">Lorem ipsum dolor <br>unde omnis iste</a></h5>
                            </div>
                            <div class="news-content">
                                <h5 class="news-text"><a href="news-single.html">Lorem ipsum dolor <br>consectetur adipiscing</a>
                                </h5>
                                <p class="news-text2"><a href="news-single.html">Sed ut perspiciatis unde omnis iste natus error sit
                                        voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                                        et quasi architecto beatae vitae dicta sunt explicabo</a></p>
                                <a href="news-single.html" class="news-link" aria-label="Read more about this news">
                                    <i class="icofont-rounded-right" aria-hidden="true"></i>
                                    <span class="sr-only">Read more</span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12 text-center">

                        <div class="button-wrapper loading-button">
                            <a class="button button-yellow" href="">
                                Load More
                                <div class="drops">
                                    <div class="drop1"></div>
                                    <div class="drop2"></div>
                                </div>
                            </a>

                            <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                <defs>
                                    <filter id="liquid">
                                        <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                                        <feColorMatrix in="blur" type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="liquid" />
                                    </filter>
                                </defs>
                            </svg>
                        </div>


                    </div>
                </div>

            </div>
        </section>
        <!-- News area end -->



    </main>
    <!-- Page content area end -->

    <!-- Gooey filter start -->
    <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop" />
            </filter>
        </defs>
    </svg>
    <!-- Gooey filter end -->

</div>
<!-- Main page wrapper end -->
<?php get_footer(); ?>