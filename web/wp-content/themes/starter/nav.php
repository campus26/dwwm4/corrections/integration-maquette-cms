    <!-- Header start -->
    <header class="page-header">

        <div class="container-fluid">
            <div class="row">

                <div class="col-xxl-12">
                    <nav class="navbar navbar-expand-lg  navbar-light header-nav d-flex">
                        <div class="logo-wrap">
                            <a class="navbar-brand" href="<?php echo get_template_directory_uri(); ?>/index.html">
                                <?php echo get_custom_logo(); ?>

                            </a>
                            <div class="mobile-nav">
                                <button class="menu-toggler" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Mobile navigation">
                                    <span class="line first"></span>
                                    <span class="line half"></span>
                                    <span class="line third"></span>
                                </button>
                            </div>
                        </div>

                        <div id="navbar" class="collapse navbar-collapse">
                            <?php
                            wp_nav_menu(array(
                                'container' => '',
                                'theme_location' => 'mainmenu',
                                'menu_class' => 'navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll header-menu', // classe CSS pour customiser mon menu
                            )); ?>

                            <!-- <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll header-menu">
                                <li class="nav-item nav-dropdown">
                                    <a href="<?php echo get_template_directory_uri(); ?>/#" class="nav-link dropdown-toggle active" id="dropdownHome" role="button" data-bs-toggle="dropdown" aria-expanded="false">home</a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownHome">

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/index.html">home</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/index.html" target="_blank">home RTL</a></li>


                                    </ul>
                                </li>
                                <li class="nav-item nav-dropdown">
                                    <a href="<?php echo get_template_directory_uri(); ?>/#" class="nav-link dropdown-toggle" id="dropdownPages" role="button" data-bs-toggle="dropdown" aria-expanded="false">Pages</a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownPages">

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/about.html">About azd</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/service-list.html">Services</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/service-single.html">Services Single</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/projects.html">Projects</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/news.html">News</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/news-single.html">News Single</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/contact.html">Contact 1</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/contact2.html">Contact 2</a></li>

                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo get_template_directory_uri(); ?>/service-list.html" class="nav-link">Services</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo get_template_directory_uri(); ?>/projects.html" class="nav-link">Projects</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo get_template_directory_uri(); ?>/contact.html" class="nav-link">contact</a>
                                </li>
                                <li class="nav-item nav-dropdown">
                                    <a href="<?php echo get_template_directory_uri(); ?>/#" class="nav-link dropdown-toggle" id="dropdownRTL" role="button" data-bs-toggle="dropdown" aria-expanded="false">RTL</a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownRTL">

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/about.html" target="_blank">About</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/service-list.html" target="_blank">Services</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/service-single.html" target="_blank">Services Single</a>
                                        </li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/projects.html" target="_blank">Projects</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/news.html" target="_blank">News</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/news-single.html" target="_blank">News Single</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/contact.html" target="_blank">Contact</a></li>

                                        <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/rtl/contact2.html" target="_blank">Contact</a></li>

                                    </ul>
                                </li>
                            </ul> -->

                        </div>
                        <div class="header-contact m-0">
                            <div class="search-wrap">
                                <a href="<?php echo get_template_directory_uri(); ?>/#search" class="icon button-open">
                                    <i class="icofont-search"></i>
                                </a>
                            </div>
                            <!-- Search form overlay -->
                            <div id="search" class="overlay hiding">
                                <button class="button-icon button-close">
                                    <i class="icofont-close-circled"></i>
                                </button>
                                <form action="#" class="form-search">
                                    <label for="keywords" class="visually-hidden">Search</label>
                                    <input class="input input-search" id="keywords" name="keywords" type="search" placeholder="Enter Keyword …" autocapitalize="off" required>
                                    <button type="submit" class="button-icon button-search">
                                        <i class="icofont-search-2"></i>
                                    </button>
                                </form>
                            </div>
                            <!-- Search form overlay -->

                            <!-- <div class="dropdown language-switch">
                                <a class="nav-link dropdown-toggle" href="<?php echo get_template_directory_uri(); ?>/#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    English
                                </a>

                                <ul class="dropdown-menu dropdown-menu-lg-start">
                                    <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/#">Arabic</a></li>
                                    <li><a class="dropdown-item" href="<?php echo get_template_directory_uri(); ?>/#">French</a></li>
                                </ul>
                            </div> -->

                            <div class="button-wrapper d-none d-xl-block">
                                <a class="button button-yellow" href="<?php echo get_template_directory_uri(); ?>/contact.html">Contact us</a>
                            </div>


                        </div>
                    </nav>
                </div>
            </div>
        </div>

    </header>
    <!-- Header end -->