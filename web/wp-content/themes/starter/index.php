<?php get_header(); ?>


<!-- Main page wrapper start -->
<div class="page-wrapper">



    <?php get_template_part('nav'); ?>

    <!-- Page content area start -->
    <main>

        <?php get_template_part('template-parts/banner'); ?>


        <?php get_template_part('template-parts/service'); ?>
    </main>
    <!-- Page content area end -->

    <!-- Gooey filter start -->
    <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop" />
            </filter>
        </defs>
    </svg>
    <!-- Gooey filter end -->

</div>
<!-- Main page wrapper end -->
<?php get_footer(); ?>